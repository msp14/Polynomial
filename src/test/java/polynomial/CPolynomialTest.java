package polynomial;

import org.junit.Test;
import org.omg.CORBA.INV_POLICY;

import static org.junit.Assert.*;

/**
 * Polynomial test routines
 *
 * @author Marcel Spitzer
 * @date: 7/24/15
 */
public class CPolynomialTest
{
    @Test
    public void testPolynomial()
    {
        final IPolynomial p = new CPolynomial(2, -1, 3);
        final IPolynomial q = new CPolynomial(0, 5, 0, -1, 0);
        final IPolynomial z = new CPolynomial(5, 0, -1, 0);

        final IPolynomial sum = new CPolynomial(5, 2, -2, 3);
        final IPolynomial diff = new CPolynomial(-5, 2, 0, 3);
        final IPolynomial scmult = new CPolynomial(6, -3, 9);
        final IPolynomial mult = new CPolynomial(10, -5, 13, 1, -3, 0);
        final IPolynomial zero1 = new CPolynomial();
        final IPolynomial zero2 = new CPolynomial(0,0,0,0,0,0);
        final IPolynomial one1 = new CPolynomial(1);
        final IPolynomial one2 = new CPolynomial(0,0,0,1);

        assertEquals(q, z);
        assertEquals(2, p.degree());
        assertEquals(3, q.degree());
        assertEquals(q.degree(), z.degree());
        assertEquals(p.add(q), sum);
        assertEquals(p.multiply(3), scmult);
        assertEquals(p.subtract(q), diff);
        assertEquals(q.subtract(p), diff.multiply(-1));
        assertEquals(new CPolynomial(4, -1), p.derive());
        assertEquals(new CPolynomial(15, 0, -1), q.derive());
        assertEquals(new CPolynomial(15, 0, -1), z.derive());

        assertTrue(p.subtract(p).isZero() && q.subtract(q).isZero());
        assertTrue(zero1.isZero() && zero2.isZero() && zero1.equals(zero2) && zero1.degree() == 0);
        assertTrue(one1.isOne() && one2.isOne() && one1.equals(one2) && one1.degree() == 0);
        assertTrue(Math.abs(p.value(2) - 9) < CPolynomial.EPSILON);
        assertEquals(p.multiply(q), mult);

        assertTrue( CPolynomial.ZERO.getCoefficient(2) < CPolynomial.EPSILON );

    }
}
