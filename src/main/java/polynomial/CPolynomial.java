package polynomial;

import java.util.*;

/**
 * Class for polynomials.
 *
 * @author Marcel Spitzer
 * @date: 7/24/15
 */
public class CPolynomial implements IPolynomial
{
    /**
     * map storing indices and coefficients
     */
    private final NavigableMap<Integer, Double> m_coefficients = new TreeMap<Integer, Double>();
    /**
     * epsilon to compare floating point numbers
     */
    public static final double EPSILON = 1E-12;
    /**
     * zero polynomial
     */
    public static final CPolynomial ZERO = new CPolynomial();
    /**
     * one polynomial
     */
    public static final CPolynomial ONE = new CPolynomial(1);

    /**
     * constructor with coefficients specified
     *
     * @param coefficients
     */
    public CPolynomial( final double... coefficients )
    {
        for (int i=0; i < coefficients.length; i++ )
            if ( coefficients[i] != 0.0 )
                m_coefficients.put( coefficients.length - i - 1, coefficients[i] );
    }

    /**
     * private ctor with map specified
     *
     * @param coefficients
     */
    private CPolynomial( final Map<Integer, Double> coefficients )
    {
        m_coefficients.putAll(coefficients);
    }

    @Override
    public int degree()
    {
        if (this.isZero())
            return 0;

        return m_coefficients.lastKey();
    }

    @Override
    public IPolynomial add( final IPolynomial other)
    {
        if (this.isZero())
            return other;

        if (other.isZero())
            return this;

        // union of indices
        final Set<Integer> indices = new HashSet<Integer>() {{
            addAll(m_coefficients.keySet());
            addAll(other.getIndices());
        }};

        // sum up coefficients
        final Map<Integer, Double> coefficients = new HashMap<Integer, Double>();
        for ( final int index : indices )
        {
            final double sum = this.getCoefficient(index) + other.getCoefficient(index);
            if ( Math.abs( sum ) > EPSILON )
                coefficients.put(index, sum);
        }

        return new CPolynomial(coefficients);
    }

    @Override
    public IPolynomial subtract( final IPolynomial other )
    {
        return this.add( other.multiply(-1.0) );
    }

    @Override
    public IPolynomial multiply( final IPolynomial other)
    {
        if (this.isZero() || other.isZero() )
            return ZERO;

        if (this.isOne())
            return other;

        if (other.isOne())
            return this;

        // calculate coefficients using multiplication formula
        final Map<Integer, Double> coefficients = new HashMap<Integer, Double>();
        for (int i = 0; i <= this.degree() + other.degree(); ++i)
            for (int k = 0; k <= i; ++k)
            {
                double curValue = !coefficients.containsKey(i) ? 0.0 : coefficients.get(i);
                double newValue = curValue + this.getCoefficient(k) * other.getCoefficient(i - k);

                // update coefficient
                coefficients.remove( i );
                if ( Math.abs( newValue ) > EPSILON )
                    coefficients.put( i, newValue );
            }

        return new CPolynomial(coefficients);
    }

    @Override
    public IPolynomial multiply( final double scalar )
    {
        if (scalar == 0.0 || this.isZero())
            return ZERO;

        return new CPolynomial( new HashMap<Integer, Double>(){{
            for (Map.Entry<Integer,Double> entry : m_coefficients.entrySet() )
                put(entry.getKey(), entry.getValue() * scalar);
        }});
    }

    @Override
    public IPolynomial derive()
    {
        return new CPolynomial( new HashMap<Integer, Double>(){{
            for ( final Map.Entry<Integer, Double> entry : m_coefficients.entrySet())
            {
                final Integer index = entry.getKey();
                if ( index == 0 )
                    continue;

                put( index - 1, entry.getValue() * index);
            }
        }});
    }

    @Override
    public boolean isZero()
    {
        return this.equals(ZERO);
    }

    @Override
    public boolean isOne()
    {
        return this.equals(ONE);
    }

    @Override
    public double value( final double x)
    {
        if (this.isZero())
            return 0.0;

        // evaluate polynomial using Horner's method
        double res = 0.0;
        for ( int i = this.degree(); i >= 0; --i )
            res = res * x + this.getCoefficient(i);

        return res;
    }

    @Override
    public int hashCode()
    {
        return m_coefficients.hashCode();
    }

    @Override
    public boolean equals( final Object obj)
    {
        return this.hashCode() == obj.hashCode();
    }

    @Override
    public Set<Integer> getIndices()
    {
        return m_coefficients.keySet();
    }

    @Override
    public double getCoefficient(final int i)
    {
        if (i < 0)
            throw new IllegalArgumentException("coefficient index must not be negative");

        if ( !m_coefficients.keySet().contains(i) )
            return 0.0;

        return m_coefficients.get(i);
    }

    @Override
    public String toString()
    {
        if (this.isZero())
            return "0.0";

        String res = "";
        for ( int i : m_coefficients.descendingKeySet() )
        {
            final double coef = m_coefficients.get(i);
            if (coef != 0)
                res = res.concat((coef < 0 ? " - " : " + ") + Math.abs(coef) + ((i > 1) ? " x^" + i : ((i == 1) ? " x" : "")));
        }

        return res;
    }
}
