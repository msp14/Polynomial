package polynomial;

import java.util.Map;
import java.util.Set;

/**
 * Interface for polynomials
 *
 * @author Marcel Spitzer
 * @date: 7/24/15
 */
public interface IPolynomial
{
    /**
     * Returns the polynomial's degree.
     */
    int degree();

    /**
     * Adds two polynomials.
     *
     * @param other polynomial to add
     * @return sum of two polynomials
     */
    IPolynomial add( final IPolynomial other );

    /**
     * Subtracts two polynomials.
     *
     * @param other polynomial to subtract
     * @return difference of two polynomials
     */
    IPolynomial subtract( final IPolynomial other );

    /**
     * Multiplies two polynomials.
     *
     * @param other polynomial to multiply with
     * @return product of two polynomials
     */
    IPolynomial multiply( final IPolynomial other );

    /**
     * Multiplies with a scalar.
     *
     * @param scalar number to multiply with
     * @return scaled polynomial
     */
    IPolynomial multiply( final double scalar );

    /**
     * calculates the first derivation
     *
     * @return first derivation
     */
    IPolynomial derive();

    /**
     * Check if polynomial is zero
     *
     * @return true if zero
     */
    boolean isZero();

    /**
     * Check if polynomial is one
     *
     * @return true if one
     */
    boolean isOne();

    /**
     * Evaluates the polynomial at given x.
     *
     * @param x input
     * @return polynomial's value
     */
    double value( final double x );

    /**
     * Returns indices of non-zero coefficients
     *
     * @return
     */
    Set<Integer> getIndices();

    /**
     * return ith coefficient
     *
     * @param i
     * @return
     */
    double getCoefficient(final int i);
}
